;;;; cl-ecmwf.asd

(asdf:defsystem #:cl-ecmwf
  :serial t
  :description "Describe cl-ecmwf here"
  :author "Your Name <your.name@example.com>"
  :license "Specify license here"
  :depends-on (#:drakma
               #:cl-json
               #:st-json
               #:cl-ppcre
               #:trivial-shell)
  :components ((:file "package")
               (:file "cl-ecmwf")))

