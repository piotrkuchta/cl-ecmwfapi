cl-ecmwfwpi is an ECMWF REST API library for Common Lisp.

Documentation of ECWMF REST API is available at 
https://software.ecmwf.int/wiki/pages/viewpage.action?pageId=22907895

You need to install quicklisp to run it: http://www.quicklisp.org/
