; cl-ecmwf.lisp

;;; cl-ecmwfwpi is an ECMWF REST API library for Common Lisp.
;;; You need to install quicklisp to run it: http://www.quicklisp.org/
;;; Piotr Kuchta, March 2013

(in-package #:cl-ecmwf)

(defun open-browser (url) (trivial-shell:shell-command (format nil "open ~a" url)))

(defun json-get(key json) (st-json:getjso key json))
(defun read-json(stream) (st-json:read-json stream))
;(defun write-json-to-string (o) (st-json:write-json-to-string o))
(defun write-json-to-string (o) (json:encode-json-to-string o))

(defvar *rc* (merge-pathnames (user-homedir-pathname) ".ecmwfapirc"))
(defvar *user* (with-open-file (s *rc*) (read-json s)))

(defvar *api-url* (json-get "url" *user*))
(defvar *api-key* (json-get "key" *user*))
(defvar *api-email* (json-get "email" *user*))

(defvar *buffer-size* (* 2 16 1024 1024))

(setf drakma:*header-stream* *standard-output*)

(defun ecmwf-uri (api) (concatenate 'string *api-url* api))

(defun http-post (api request &key (want-stream t))
  (let ((cookie-jar (make-instance 'drakma:cookie-jar))
        (extra-headers (list (cons "From" *api-email*)
                             (cons "X-ECMWF-KEY" *api-key*)))
        (url (concatenate 'string *api-url* api "/requests"))
        (content (write-json-to-string request)))
       (drakma:http-request url
                       :additional-headers extra-headers
                       :accept "application/json"
                       :method :post
                       :content-type "application/json; charset=utf-8"
                       :redirect nil
                       :cookie-jar cookie-jar
                       :content content
                       :want-stream want-stream)))

(defun http-get (uri &key (want-stream t))
  (let ((cookie-jar (make-instance 'drakma:cookie-jar))
        (extra-headers (list (cons "From" *api-email*)
                             (cons "X-ECMWF-KEY" *api-key*))))
     (drakma:http-request uri
                      :accept "application/json"
                      :redirect nil
                      :additional-headers extra-headers
                      :cookie-jar cookie-jar
                      :want-stream want-stream)))

(defun copy-stream (in out &key (element-type '(unsigned-byte 8)))
  (let ((seq (make-array *buffer-size* :element-type element-type
                                       :adjustable t
                                       :fill-pointer *buffer-size*)))
      (loop
        (setf (fill-pointer seq) (read-sequence seq in))
        (when (zerop (fill-pointer seq))
          (return))
        (write-sequence seq out))))

(defun log-info (&rest parts)
  (multiple-value-bind
	(second minute hour date month year day-of-week dst-p tz)
	(get-decoded-time)
    (format t "~&~d-~2,'0d-~d ~2,'0d:~2,'0d:~2,'0d (GMT~@d) "
              month date year hour minute second (- tz)))
  (apply #'format t parts)
  (format t "~&"))

(defun log-status (status-code reason-phrase)
  (log-info "~d ~a" status-code reason-phrase))

(defun delay (seconds)
  (when (> seconds 0)
	(log-info "Retrying in ~d seconds." seconds)
    (sleep seconds)))

(defun messages (stream)
  (let ((j (read-json stream)))
    (flet ((value (a) (or (json-get a j) "")))
      (format t "~a~&~a~&" (value "messages") (value "error")))))

(defun get-file (url target retry-after)
  (log-info "Download ~a from ~a" target url)
  (delay retry-after)
  (log-info "GET ~a" url)
  (multiple-value-bind
    (body-or-stream status-code headers uri stream must-close reason-phrase)
    (http-get url)
    (progn
      (log-status status-code reason-phrase)
      (cond
        ((= 200 status-code)
           (progn
             (log-info "Downloading file ~a." target)
             (with-open-file (out target :direction :output :if-exists :supersede
                                         :element-type '(unsigned-byte 8))
               (copy-stream body-or-stream out))
               t))
        ((or (= 202 status-code) 
             (= 303 status-code)
             (= 503 status-code)) (progn
                                    (messages body-or-stream)
                                    (get-file (cdr (assoc :location headers))
                                              target
                                              retry-after)))
        (t  (progn
              (open-browser (format nil "~a?key=~a" url *api-key*))
              (messages body-or-stream)
              (log-info "ERROR")))))))

(defun news (api)
  (let ((j (api-get (concatenate 'string api "/news"))))
    (log-info "~a" (json-get "news" j))))

(defun greetings ()
  (let* ((w (api-get "/who-am-i"))
         (full-name (json-get "full_name" w))
         (email (json-get "email" w)))
    (progn
      (log-info "ECMWF API Common Lisp library 1.0")
      (log-info "Welcome ~a ~a" full-name email))))

(defun api-get (api)
  "Returns decoded json response on GET request to the given api (URI). "
  (log-info "GET ~a" (ecmwf-uri api))
  (multiple-value-bind
    (body-or-stream status-code headers uri stream must-close reason-phrase)
    (http-get (ecmwf-uri api))
    (progn
      (log-status status-code reason-phrase)
      (read-json body-or-stream))))

(defun api-post (api request)
  "Returns decoded json response on POST request to the given api (URI). "
  (log-info "POST ~s" (ecmwf-uri api))
  (multiple-value-bind
    (body-or-stream status-code headers uri stream must-close reason-phrase)
    (http-post api request)
    (progn
      (log-status status-code reason-phrase)
      (read-json body-or-stream))))

(defun do-retrieve (api request retry-after)
  (delay retry-after)
  (multiple-value-bind
    (body-or-stream status-code headers uri stream must-close reason-phrase)
    (http-post api request)
    (let ((ra (or (parse-integer (cdr (assoc :retry-after headers)))
                  retry-after)))
      (progn
        (log-status status-code reason-phrase)
        (cond
          ((or (= 202 status-code)
               (= 303 status-code)) (progn
                                      (messages body-or-stream)
                                      (get-file
                                      (cdr (assoc :location headers))
                                      (cdr (assoc "target" request :test #'equal))
                                      ra)))
          ((= 503 status-code) (do-retrieve api request ra))
          (t (progn ; 400
               (log-info "Retrieve FAILED:")
               (messages body-or-stream))))))))

(defun retrieve (api request)
  "Retrieves binary file specified as :target in request from a given api"
  (greetings)
  (news api)
  (do-retrieve api request 0))

