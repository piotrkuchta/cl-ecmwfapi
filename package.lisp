;;;; package.lisp

(defpackage #:cl-ecmwf
  (:use #:cl)
  (:export :retrieve
           :api-get
           :api-post))

